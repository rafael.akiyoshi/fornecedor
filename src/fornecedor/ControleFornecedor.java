package fornecedor;

import java.util.ArrayList;

public class ControleFornecedor {

    
    private ArrayList<Fornecedor> listaFornecedores;
    
    public ControleFornecedor(){
        this.listaFornecedores = new ArrayList<Fornecedor>();
    }
    public String adicionarFornecedorFisico(PessoaFisica fornecedor){
        listaFornecedores.addAll(this);
        return "Fornecedor Fisico Adicionado";
    }
    
    public String adicionarFornecedorJuridico(PessoaJuridica fornecedor){
        listaFornecedores.addAll(this);
        return "Fornecedor Juridico Adicionado";
    }
        
    
    public String pesquisarFornecedor(String umNome){
        for(Fornecedor umFornecedor: listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(umNome))
                return umFornecedor.getNome();
        }
        return "O Fornecedor nao foi encontrado!";
    }
    
    public String removerFornecedor(String umNome){
        for(Fornecedor umFornecedor: listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(umNome)){
                listaFornecedores.remove(umFornecedor);
                return "Fornecedor Removido";
            }
        }
        return "Fornecedor nao encontrado";
    }

    
}
